# autoload-search

Search for Composer vendor/autoload.php

# Examples of use
* https://gitlab.com/php-utils/ansi-to-html-cli

# Old name in Packagist
* [php-util/autoload-search
  ](https://packagist.org/packages/php-util/autoload-search)
